package com.example.appventas.model;

//Clase model Artículo
public class Articulo {

    public String titulo, imagen;
    String condition;
    String price;
    String id;
    String ubicacion;
    String descripcion;

    public Articulo(String titulo, String imagen, String condition, String price, String id, String ubicacion, String descripcion) {
        this.titulo = titulo;
        this.imagen = imagen;
        this.condition = condition;
        this.price = price;
        this.id = id;
        this.ubicacion = ubicacion;
        this.descripcion = descripcion;
    }

    //Getters y setters
    public String getTitulo() {
        return titulo;
    }

    public String getImagen() {
        return imagen;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public String getCondition() {
        return condition;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getPrice(){ return price; }

    public String getId() {
        return id;
    }
}


