package com.example.appventas.fragments.lista_articulos;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.appventas.R;
import com.google.android.material.textfield.TextInputLayout;

//Fragment principal, búsqueda del artículo
public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;
    private Context context;
    //Artículo a buscar por el usuario
    public static String inputArticulo;

    //Se crea la vista con el layout fragment_home
    public View onCreateView(@NonNull LayoutInflater inflater,
                             final ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        final TextInputLayout articulo = root.findViewById(R.id.articulo);
        final TextView textView = root.findViewById(R.id.text_home);
        final Button buttonBuscar = root.findViewById(R.id.button);
        //Se setea el error para el articulo TextInputLayout
        articulo.setError(null);

        //La vista modelo home
        homeViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);

            }
        });

        //Acción botón buscar
        buttonBuscar.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                //Se toma el articulo a buscar por el usuario
                inputArticulo = String.valueOf(articulo.getEditText().getText());

                //Si el campo artículo se encuentra vacío arrojará un error en pantalla
                if((inputArticulo.equals(null))||(inputArticulo.equals(""))){
                    articulo.setError("Introduzca un articulo");
                }
                //Si el campo artículo está lleno, entonces se pasa el parámetro de búsqueda y se invoca al próximo fragment
                else{
                    buttonBuscar.setClickable(true);
                    inputArticulo = String.valueOf(articulo.getEditText().getText());
                    // TODO Auto-generated method stub
                    Toast.makeText(getContext(),"Buscando tu articulo",Toast.LENGTH_SHORT).show();

                    //Próximo fragment de la lista de artículos
                    ListaArticulos listaArticulos = new ListaArticulos();
                    getActivity().getSupportFragmentManager().beginTransaction()
                            .replace(R.id.layout_main, listaArticulos)
                            .addToBackStack(null)
                            .commit();
                }
            }
        });

        return root;
    }
}