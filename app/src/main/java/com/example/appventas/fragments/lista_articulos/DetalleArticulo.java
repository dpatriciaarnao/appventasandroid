package com.example.appventas.fragments.lista_articulos;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.appventas.R;
import com.example.appventas.model.Articulo;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

//Fragment que trae el detalle del artículo por ID, el cual fue seleccionado del listado por el usuario
public class DetalleArticulo extends Fragment {

    private RequestQueue mRequestQueue ;
    Context context;
    String idArticulo;
    String descripcionData;

    public ImageView imageView;
    public TextView titulo;
    public TextView ubicacionState;
    public TextView ubicacionCity;
    public TextView precio;
    public TextView descripcion;

    private ArrayList<Articulo> articulo;

    //Se crea la vista del layout detalle_articulo
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (container != null) {
            container.removeAllViews();
        }
        View root = inflater.inflate(R.layout.detalle_articulo, container, false);

        //Se trae desde el listado y su acción del click, el ID del artículo seleccionado
        Bundle bundle = this.getArguments();

        if(bundle != null){
            //El ID se asigna a una variable
            idArticulo = bundle.getString("id");
            Log.d("ID articulo", idArticulo);
        }

        //Se inicializa la lista array y la librería volley para buscar datos del json
        articulo = new ArrayList<>();
        mRequestQueue = Volley.newRequestQueue(getContext());

        //Método que obtiene detalle del artículo por ID
        obtenerDatosDetalle();

        //Se inicializan variables
        imageView = root.findViewById(R.id.imageView_articulo);
        titulo = root.findViewById(R.id.textView_tituloArticulo);
        precio = root.findViewById(R.id.textView_precioArticulo);
        ubicacionState = root.findViewById(R.id.textView_ubicacionArticuloState);
        ubicacionCity = root.findViewById(R.id.textView_ubicacionArticuloCity);
        descripcion = root.findViewById(R.id.textView_descripcionArticulo);

        return root;
    }

    //Método que trae el detalle del artículo seleccionado por el usuario
    private void obtenerDatosDetalle() {
        //URL más el id del artículo seleccionado
        String url = "https://api.mercadolibre.com/items/"+idArticulo;

        //Parseo del json y asignación de variables
        final ArrayList<Articulo> articulo=new ArrayList<>();
        final JsonObjectRequest peticionJSON = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                        String tituloData = response.getString("title");
                        String imagenData = response.getString("thumbnail");

                        JSONObject objectUbicacion= response.getJSONObject("location");
                        JSONObject objectState = objectUbicacion.getJSONObject("state");
                        JSONObject objectCity = objectUbicacion.getJSONObject("city");
                        String nameStateData = objectState.getString("name");
                        String nameCityData = objectCity.getString("name");

                        String moneda = response.getString("currency_id");


                        Integer precioOriginalData = (Integer) response.get("price");
                        String precioFormatData = String.format("%,d", precioOriginalData);
                        String idData = (String) response.get("id");

                        JSONArray arrayDescripcion= response.getJSONArray("descriptions");
                            for (int i=0;i<arrayDescripcion.length();i++){
                                JSONObject resultado = arrayDescripcion.getJSONObject(i);
                                descripcionData = (String) resultado.get("id");
                            }

                        //Se setean la data del json a las variables en pantalla
                        titulo.setText(tituloData);
                        ubicacionState.setText(nameStateData);
                        ubicacionCity.setText(nameCityData);
                        precio.setText(precioFormatData+moneda);
                        descripcion.setText(descripcionData);
                        Picasso.get().load(imagenData).into(imageView);

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(peticionJSON);
    }
}
