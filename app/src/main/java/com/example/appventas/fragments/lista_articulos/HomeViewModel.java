package com.example.appventas.fragments.lista_articulos;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

//Modelo vista del HomeFragment, pero sólo trae el texto de la primera fila
public class HomeViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public HomeViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("Coloca aquí tu artículo a buscar");
    }

    public LiveData<String> getText() {
        return mText;
    }
}