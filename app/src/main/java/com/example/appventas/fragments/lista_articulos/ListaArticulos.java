package com.example.appventas.fragments.lista_articulos;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.appventas.R;
import com.example.appventas.adapters.RvAdapter;
import com.example.appventas.model.Articulo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

//Segundo fragment que trae la lista de artículos de acuerdo al artículo buscado por el usuario
public class ListaArticulos extends Fragment{

    private RecyclerView mRecyclerView;
    private RvAdapter mAdapter;
    private ArrayList<Articulo> mArticulo;
    private RequestQueue mRequestQueue ;

    Context context;

    //Artículo ingresado por el usuario
    String articulo = HomeFragment.inputArticulo;


    //Se crea la vista del layout lista_articulos con su recyclerview
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (container != null) {
            container.removeAllViews();
        }

        View root = inflater.inflate(R.layout.lista_articulos, container, false);

        //Se inicializa vacío el adapter
        mArticulo = new ArrayList<>();

        mRecyclerView = root.findViewById(R.id.recycler_articulos);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        mAdapter=new RvAdapter(getContext());
        mRecyclerView.setAdapter(mAdapter);

        //Se le coloca una pequeña decoración
        RecyclerView.ItemDecoration decoration=new DividerItemDecoration(getContext(),
                DividerItemDecoration.VERTICAL);
        mRecyclerView.addItemDecoration(decoration);

        //Se inicializa la librería volley para su uso trayendo datos de un json
        mRequestQueue = Volley.newRequestQueue(getContext());
        //Se invoca este método que traerá los datos desde un json
        obtenerDatos();

        return root;
    }

    //Método que trae los datos de la lista del artículo buscado por el usuario
    private void obtenerDatos() {
        //URL
        String url = "https://api.mercadolibre.com/sites/MCO/search?q="+articulo;

        //Parseo del json y asignación de variables
        final ArrayList<Articulo> articulos=new ArrayList<>();
        JsonObjectRequest peticionJSON = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray arrayJSON = response.getJSONArray("results");
                    Log.v("test",arrayJSON.toString());

                    for (int i=0;i<arrayJSON.length();i++){

                        JSONObject resultado = arrayJSON.getJSONObject(i);
                        String titulo = (String) resultado.get("title");
                        String imagen = (String) resultado.get("thumbnail");
                        String condition = String.valueOf( resultado.get("condition"));
                        Integer precioOriginal = (Integer) resultado.get("price");
                        String precioFormat = String.format("%,d", precioOriginal);
                        String id = (String) resultado.get("id");
                        Articulo p = new Articulo(titulo,imagen,condition,precioFormat, id, "", "");
                        articulos.add(p);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                //Se setean los resultados del listado al adaptador
                mAdapter.setArticulos(articulos);
                mAdapter.notifyDataSetChanged();
                //Acción clic sobre algún ítem del listado
                mAdapter.setOnItemClickListener(new RvAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(int position) {
                        articulos.get(position);
                        Log.d("position", articulos.get(position).getId());

                        Bundle bundle = new Bundle();
                        bundle.putString("id",articulos.get(position).getId());

                        //Se invoca el siguiente fragment con el id del artículo y que traerá el detalle del ítem seleccionado
                        DetalleArticulo detalleArticulo = new DetalleArticulo();
                        detalleArticulo.setArguments(bundle);
                        getFragmentManager()
                                .beginTransaction()
                                .replace(R.id.layout_main, detalleArticulo).addToBackStack(null)
                                .commit();
                    }
                });

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(peticionJSON);
    }
}
