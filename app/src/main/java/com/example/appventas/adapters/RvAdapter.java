package com.example.appventas.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.appventas.R;

import java.util.ArrayList;

import com.example.appventas.model.Articulo;
import com.squareup.picasso.Picasso;

//Adaptador para el listado de los artículos con recyclerview
public class RvAdapter extends RecyclerView.Adapter<RvAdapter.RvAdapterViewHolder> {
    private Context mContext;
    private ArrayList<Articulo> mArticulo;
    private OnItemClickListener mListener;

    public interface OnItemClickListener{
        void onItemClick(int position);
    }

    //Escucha click sobre ítem
    public void setOnItemClickListener(OnItemClickListener listener){
        mListener= listener;

    }

    public RvAdapter(Context context)
    {
        this.mContext=context;
        mArticulo=new ArrayList<>();

    }
    public RvAdapter(Context context, ArrayList<Articulo> articulo){
        mContext = context;
        mArticulo = articulo;
    }


    //Crea la vista del layout lista_articulos_items
    @Override
    public RvAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.lista_articulos_items, parent, false);
        return new RvAdapterViewHolder(v, mListener);
    }

    //Asigna y setea las variables de pantalla
    @Override
    public void onBindViewHolder(@NonNull RvAdapterViewHolder holder, int position) {
        Articulo articulo = mArticulo.get(position);

        String imageUrl = articulo.getImagen();
        String title = articulo.getTitulo();
        String condition = articulo.getCondition();
        String price = articulo.getPrice();
        String id = articulo.getId();

        holder.titulo.setText(title);
        holder.condicion.setText(condition);
        holder.precio.setText(price);
        Picasso.get().load(imageUrl).into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return mArticulo.size();
    }

    public void setArticulos(ArrayList<Articulo> articulos)
    {
        mArticulo=articulos;
    }

    public static class RvAdapterViewHolder extends RecyclerView.ViewHolder{

        public ImageView imageView;
        public TextView titulo;
        public TextView condicion;
        public TextView precio;
        public LinearLayout linearLayout;

        public RvAdapterViewHolder(@NonNull View itemView, final OnItemClickListener listener) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imageView_foto);
            titulo = itemView.findViewById(R.id.textView_titulo);
            condicion = itemView.findViewById(R.id.textView_condition);
            precio = itemView.findViewById(R.id.textView_price);
            linearLayout = itemView.findViewById(R.id.layout_lista_articulos);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener!=null){
                        int position = getAdapterPosition();
                        if(position!=RecyclerView.NO_POSITION){
                            listener.onItemClick(position);
                        }
                    }
                }
            });


        }


    }


}

